import 'dart:convert';
import 'package:email_validator/email_validator.dart';
import 'package:expandable/expandable.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/CustomWidgets/Picker.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:fga/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;

const ht = 20.0;
const btnwidth = 230.0;

class AttendanceDetail extends StatefulWidget {
  @override
  _AttendanceDetailState createState() => _AttendanceDetailState();
}

class _AttendanceDetailState extends State<AttendanceDetail> {
  var attendances = [];

  Future<void> loadData() async {
    var url = Uri.parse(API_URL + 'api/attendance-report/');
    print("");

    http
        .get(url, headers: {"Accept": "application/json", "authentication": "Basic " + ""})
        .then((response) async {
          print(response.body);
          if (response.statusCode == 200) {
            setState(() {
              attendances = jsonDecode(response.body);
            });
          } else {
            print(jsonDecode(response.body)['detail']);
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          if ("" != "")
            Fluttertoast.showToast(
              msg: "Server Error",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
        });
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Attendance", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          itemCount: attendances.length,
          itemBuilder: (context, index) {
            return AttendanceDetailBox(attendances[index]);
          },
        ),
      ),
    );
  }
}

class AttendanceDetailBox extends StatelessWidget {
  Map<String, dynamic> data;
  AttendanceDetailBox(this.data);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: Colors.white38,
      ),
      margin: EdgeInsets.symmetric(vertical: 2),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      child: ExpandablePanel(
        header: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            data['name'],
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, fontFamily: "Nunito"),
          ),
        ),
        expanded: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white38,
          ),
          margin: EdgeInsets.symmetric(horizontal: 5),
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              Attendance(title: "Id", info: data['id'].toString()),
              Attendance(title: "Date", info: data['year'].toString() + "-" + data['month'].toString() + "-" + data['day'].toString()),
              Attendance(title: "Status", info: data['status']),
            ],
          ),
        ),
        collapsed: SizedBox(),
      ),
    );
  }
}

class Attendance extends StatelessWidget {
  String title, info;
  Attendance({this.title, this.info});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            title,
            style: TextStyle(color: Theme.of(context).accentColor, fontFamily: "Nunito", fontWeight: FontWeight.bold),
          ),
          Text(
            info,
            style: TextStyle(
              fontSize: 12,
              color: Theme.of(context).accentColor,
              fontFamily: "Nunito",
            ),
          ),
        ],
      ),
    );
  }
}
