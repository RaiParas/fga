import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FocusScreen extends StatefulWidget {
  @override
  _FocusState createState() => _FocusState();
}

class _FocusState extends State<FocusScreen> {
  String selectedFocus = null;
  TextEditingController heightController, weightController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Step 4 of 4"),
        centerTitle: true,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(height: 80),
              Text("Please follow this steps"),
              Text("to get your Fitness plan."),
              SizedBox(height: 20),
              Text("What is your want", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35)),
              Text("to focus on?", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35)),
              SizedBox(height: 20),
              SelectionButton(
                title: "Upper",
                subtitle: "/Upper",
                width: 150,
                isSelected: selectedFocus == "Upper",
                onTap: () {
                  setState(() {
                    selectedFocus = "Upper";
                  });
                },
              ),
              Divider(thickness: 1, height: 15),
              SizedBox(height: 40),
              SelectionButton(
                title: "Belly",
                subtitle: "Belly",
                width: 150,
                isSelected: selectedFocus == "Belly",
                onTap: () {
                  setState(() {
                    selectedFocus = "Belly";
                  });
                },
              ),
              Divider(thickness: 1, height: 15),
              SizedBox(height: 40),
              SelectionButton(
                title: "Lower",
                subtitle: "Lower",
                width: 150,
                isSelected: selectedFocus == "Lower",
                onTap: () {
                  setState(() {
                    selectedFocus = "Lower";
                  });
                },
              ),
              Divider(height: 15, thickness: 1),
              SizedBox(height: 200),
              NextStepButton(
                isEnabled: this.selectedFocus != null,
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
