import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Level extends StatefulWidget {
  @override
  _LevelState createState() => _LevelState();
}

class _LevelState extends State<Level> {
  String selectedLevel = null;
  TextEditingController heightController, weightController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Step 3 of 4"),
        centerTitle: true,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(height: 80),
              Text("Please follow this steps"),
              Text("to get your Fitness plan."),
              SizedBox(height: 50),
              Text("What is your level?", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35)),
              SizedBox(height: 20),
              SelectionButton(
                title: "Beginner",
                subtitle: "/Beginner",
                width: 150,
                isSelected: selectedLevel == "Beginner",
                onTap: () {
                  setState(() {
                    selectedLevel = "Beginner";
                  });
                },
              ),
              Divider(thickness: 1, height: 15),
              SizedBox(height: 40),
              SelectionButton(
                title: "Intermediate",
                subtitle: "Intermediate",
                width: 150,
                isSelected: selectedLevel == "Intermediate",
                onTap: () {
                  setState(() {
                    selectedLevel = "Intermediate";
                  });
                },
              ),
              Divider(thickness: 1, height: 15),
              SizedBox(height: 40),
              SelectionButton(
                title: "Advanced",
                subtitle: "Advanced",
                width: 150,
                isSelected: selectedLevel == "Advanced",
                onTap: () {
                  setState(() {
                    selectedLevel = "Advanced";
                  });
                },
              ),
              Divider(height: 15, thickness: 1),
              SizedBox(height: 200),
              NextStepButton(
                isEnabled: this.selectedLevel != null,
                onTap: () {
                  Navigator.pushNamed(context, '/focus');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
