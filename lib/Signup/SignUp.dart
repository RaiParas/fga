import 'package:email_validator/email_validator.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  void signup() {
    Fluttertoast.showToast(
        msg: "Logging in",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 12.0);
    var url = Uri.parse(API_URL + 'api/rest-auth/registration/');
    http
        .post(url, headers: {
          "Accept": "application/json"
        }, body: {
          'username': nameController.text,
          'email': emailController.text,
          'password1': passwordController.text,
          'password2': passwordController.text,
        })
        .then((response) {
          if (response.statusCode == 201) {
            Fluttertoast.showToast(
              msg: "Signed up, please signin",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 12.0,
            );
            Navigator.pushNamed(context, '/signin');
          } else {
            Fluttertoast.showToast(
              msg: (response.body),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
          print('Response status: ${response.statusCode}');
          print('Response body: ${response.body}');
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          Fluttertoast.showToast(
            msg: "Try again",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  bool isAgree = false;
  final nameController = TextEditingController(),
      emailController = TextEditingController(),
      ageController = TextEditingController(),
      wtController = TextEditingController(),
      htController = TextEditingController(),
      passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign Up"),
        elevation: 1,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Center(
          child: Container(
            alignment: Alignment.center,
            width: 400,
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Image.asset(
                    'assets/images/logo.png',
                    height: 180,
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.1)),
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      children: [
                        InputForSignInUp(
                          controller: nameController,
                          title: "Username:",
                          hint: "eg. hero",
                          validator: (value) {
                            if (value == "") return "Username cannot be empty";
                          },
                        ),
                        SizedBox(height: 10),
                        InputForSignInUp(
                          controller: emailController,
                          title: "Email:",
                          hint: "eg. eg@email.com",
                          validator: (value) {
                            if (value == "")
                              return "Email cannot be empty";
                            else if (!EmailValidator.validate(value)) return "Enter valid email";
                          },
                        ),
                        SizedBox(height: 10),
                        InputForSignInUp(
                          controller: passwordController,
                          title: "Password:",
                          hint: "****",
                          keybordType: TextInputType.visiblePassword,
                          onsecuretext: true,
                          validator: (value) {
                            if (value == "") return "Password cannot be empty";
                          },
                        ),
                        SizedBox(height: 10),
                        InputForSignInUp(
                          title: "Re password:",
                          hint: "****",
                          keybordType: TextInputType.visiblePassword,
                          onsecuretext: true,
                          validator: (value) {
                            if (value == "")
                              return "Password cannot be empty";
                            else if (passwordController.text == null)
                              return "Above password cannot be empty";
                            else if (passwordController.text != value) return "Password doesnot match";
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Checkbox(
                                value: isAgree,
                                activeColor: Theme.of(context).primaryColor,
                                onChanged: (value) {
                                  setState(() {
                                    isAgree = value;
                                  });
                                }),
                            Text("I agree to the ", style: TextStyle(fontFamily: "Nunito")),
                            TextButton(onPressed: () {}, child: Text("Terms and conditions"))
                          ],
                        ),
                        SizedBox(height: 40),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  MyOutlinedButton(
                    label: "Sign up",
                    onTap: () {
                      if (!isAgree) {
                        Fluttertoast.showToast(msg: "Terms and condition not agreed", backgroundColor: Colors.red);
                        return;
                      }
                      if (isAgree && _formKey.currentState.validate()) signup();
                    },
                  ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Already a member? ", style: TextStyle(fontFamily: "Nunito")),
                      TextButton(onPressed: () {}, child: Text("Sign In", style: TextStyle(color: Theme.of(context).accentColor.withOpacity(0.4))))
                    ],
                  ),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ),
        ),
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }
}
