import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class GenderSelection extends StatefulWidget {
  @override
  _GenderSelectionState createState() => _GenderSelectionState();
}

class _GenderSelectionState extends State<GenderSelection> {
  String selectedGender = null;
  TextEditingController heightController, weightController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Step 1 of 4"),
        centerTitle: true,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(height: 80),
              Text("Please follow this steps"),
              Text("to get your Fitness plan."),
              SizedBox(height: 50),
              Text("Gender", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35)),
              SizedBox(height: 20),
              SelectionButton(
                title: "Male",
                width: 150,
                isSelected: selectedGender == "Male",
                onTap: () {
                  setState(() {
                    selectedGender = "Male";
                  });
                },
              ),
              SizedBox(height: 10),
              SelectionButton(
                title: "Female",
                width: 150,
                isSelected: selectedGender == "Female",
                onTap: () {
                  setState(() {
                    selectedGender = "Female";
                  });
                },
              ),
              SizedBox(height: 100),
              InputForHW(
                controller: heightController,
                title: "Height",
                hint: "0 cm",
                keybordType: TextInputType.number,
              ),
              Divider(height: 0, thickness: 1),
              InputForHW(
                controller: weightController,
                title: "Weight",
                hint: "0 kg",
                keybordType: TextInputType.number,
              ),
              SizedBox(height: 160),
              NextStepButton(
                isEnabled: this.selectedGender != null,
                onTap: () {
                  Navigator.pushNamed(context, '/goal');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
