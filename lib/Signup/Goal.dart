import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Goal extends StatefulWidget {
  @override
  _GoalState createState() => _GoalState();
}

class _GoalState extends State<Goal> {
  String selectedGoal = null;
  TextEditingController heightController, weightController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Step 2 of 4"),
        centerTitle: true,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(height: 80),
              Text("Please follow this steps"),
              Text("to get your Fitness plan."),
              SizedBox(height: 50),
              Text("What is your goal?", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35)),
              SizedBox(height: 20),
              SelectionButton(
                title: "Lose weight",
                width: 150,
                isSelected: selectedGoal == "Lose weight",
                onTap: () {
                  setState(() {
                    selectedGoal = "Lose weight";
                  });
                },
              ),
              Divider(height: 50, thickness: 1),
              SelectionButton(
                title: "Get toned",
                width: 150,
                isSelected: selectedGoal == "Get toned",
                onTap: () {
                  setState(() {
                    selectedGoal = "Get toned";
                  });
                },
              ),
              Divider(height: 50, thickness: 1),
              SelectionButton(
                title: "Build muscle",
                width: 150,
                isSelected: selectedGoal == "Build muscle",
                onTap: () {
                  setState(() {
                    selectedGoal = "Build muscle";
                  });
                },
              ),
              Divider(height: 50, thickness: 1),
              SizedBox(height: 175),
              NextStepButton(
                isEnabled: this.selectedGoal != null,
                onTap: () {
                  Navigator.pushNamed(context, '/level');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
