import 'dart:convert';
import 'package:email_validator/email_validator.dart';
import 'package:expandable/expandable.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/CustomWidgets/Picker.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:fga/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;

const ht = 20.0;
const btnwidth = 230.0;

class Plans extends StatefulWidget {
  @override
  _PlansState createState() => _PlansState();
}

class _PlansState extends State<Plans> {
  var plans = [];

  Future<void> loadData() async {
    print(API_URL);
    var url = Uri.parse(API_URL + 'api/plan/');

    http
        .get(url, headers: {"Accept": "application/json", "authentication": "Basic " + ""})
        .then((response) async {
          print(response.body);
          if (response.statusCode == 200) {
            setState(() {
              plans = jsonDecode(response.body);
            });
          } else {
            print(jsonDecode(response.body)['detail']);
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          if ("" != "")
            Fluttertoast.showToast(
              msg: "Server Error",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
        });
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Plans", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: true,
        actions: [
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return Dialog(
                        insetPadding: EdgeInsets.all(0),
                        child: SingleChildScrollView(
                          child: AddPlanBox(
                            reload: loadData,
                          ),
                        ),
                      );
                    });
              }),
        ],
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          itemCount: plans.length,
          itemBuilder: (context, index) {
            return PlanDetailBox(plans[index]);
          },
        ),
      ),
    );
  }
}

class PlanDetailBox extends StatelessWidget {
  Map<String, dynamic> data;
  PlanDetailBox(this.data);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: Colors.white38,
      ),
      margin: EdgeInsets.symmetric(vertical: 2),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      child: ExpandablePanel(
        header: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            data['name'],
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, fontFamily: "Nunito"),
          ),
        ),
        expanded: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white38,
          ),
          margin: EdgeInsets.symmetric(horizontal: 5),
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              PlanDetail(title: "Id", info: data['id'].toString()),
              PlanDetail(title: "Amount", info: data['amount']),
              PlanDetail(title: "Duration", info: data['duration']),
            ],
          ),
        ),
        collapsed: SizedBox(),
      ),
    );
  }
}

class PlanDetail extends StatelessWidget {
  String title, info;
  PlanDetail({this.title, this.info});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            title,
            style: TextStyle(color: Theme.of(context).accentColor, fontFamily: "Nunito", fontWeight: FontWeight.bold),
          ),
          Text(
            info,
            style: TextStyle(
              fontSize: 12,
              color: Theme.of(context).accentColor,
              fontFamily: "Nunito",
            ),
          ),
        ],
      ),
    );
  }
}

class AddPlanBox extends StatefulWidget {
  Function reload;
  AddPlanBox({this.reload});
  @override
  _AddPlanBoxState createState() => _AddPlanBoxState();
}

class _AddPlanBoxState extends State<AddPlanBox> {
  final nameController = TextEditingController();

  final amountController = TextEditingController();

  final durationController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  void saveMember() {
    var url = Uri.parse(API_URL + 'api/plan/');

    Fluttertoast.showToast(
      msg: "Saving",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.orange,
      textColor: Colors.white,
      fontSize: 12.0,
    );
    http
        .post(url, headers: {
          "Accept": "application/json"
        }, body: {
          "name": nameController.text,
          "amount": amountController.text,
          "duration": durationController.text,
        })
        .then((response) async {
          print((response.statusCode));

          if (response.statusCode == 201) {
            widget.reload();
            Navigator.pop(context);
          } else {
            print(jsonDecode(response.body)['detail']);
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          Fluttertoast.showToast(
            msg: "Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.transparent,
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(0),
                margin: EdgeInsets.all(0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.1)),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 30),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Add Plans",
                        style: TextStyle(fontFamily: "Nunito", fontWeight: FontWeight.bold, fontSize: 17),
                      ),
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: nameController,
                      title: "Name:",
                      hint: "eg. Loose weight",
                      validator: (value) {
                        if (value == null || value == '') return "Name cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: amountController,
                      title: "Amount:",
                      hint: "eg. Rs. 2000",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Amount cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: durationController,
                      title: "Duration:",
                      hint: "eg. 3 hr",
                      keybordType: TextInputType.text,
                      validator: (value) {
                        if (value == null || value == '') return "Duration cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              SizedBox(height: 10),
              MyOutlinedButton(
                label: "Submit",
                onTap: () {
                  if (_formKey.currentState.validate()) saveMember();
                },
              ),
              SizedBox(height: 10),
              MyOutlinedButton(
                label: "Cancel",
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
