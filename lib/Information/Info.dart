import 'dart:convert';

import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:flutter/material.dart';

const ht = 20.0;
const btnwidth = 230.0;

class Info extends StatefulWidget {
  @override
  _InfoState createState() => _InfoState();
}

class _InfoState extends State<Info> {
  List _data = [];
  Future<void> loadJson() async {
    final jsonData = await json.decode(await DefaultAssetBundle.of(context).loadString("assets/detail.json"));
    var data = jsonData;
    setState(() {
      _data = data;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          itemCount: _data.length,
          itemBuilder: (context, index) => Column(
            children: [
              SizedBox(height: ht),
              MyOutlinedButton(
                width: btnwidth,
                label: _data[index]['title'],
                onTap: () {
                  Navigator.pushNamed(context, '/infodetail', arguments: [_data[index]['title'], _data[index]['subtitle']]);
                },
              ),
            ],
          ),
        ),
        //  Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: [
        // SizedBox(height: ht),
        // MyOutlinedButton(
        //   width: btnwidth,
        //   label: "Shoulder",
        //   onTap: () {},
        // ),
        // SizedBox(height: ht),
        // MyOutlinedButton(
        //   width: btnwidth,
        //   label: "Back",
        //   onTap: () {},
        // ),
        // SizedBox(height: ht),
        // MyOutlinedButton(
        //   width: btnwidth,
        //   label: "Biceps",
        //   onTap: () {},
        // ),
        // SizedBox(height: ht),
        // MyOutlinedButton(
        //   width: btnwidth,
        //   label: "Tricep",
        //   onTap: () {},
        // ),
        // SizedBox(height: ht),
        // MyOutlinedButton(
        //   width: btnwidth,
        //   label: "Legs",
        //   onTap: () {},
        // ),
        // SizedBox(height: ht),
        // MyOutlinedButton(
        // width: btnwidth,
        //   label: "Abs",
        //   onTap: () {},
        // ),

        // ],
        // ),
      ),
    );
  }
}
