import 'dart:io';

import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

const ht = 20.0;
const btnwidth = 230.0;

class Detailing extends StatefulWidget {
  @override
  _DetailingState createState() => _DetailingState();
}

class _DetailingState extends State<Detailing> {
  List tabs;
  PageController _controller = PageController(
    initialPage: 0,
  );
  bool mainPage = true;
  Map<String, dynamic> detail;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<dynamic> args = ModalRoute.of(context).settings.arguments as List<dynamic>;
    tabs = args[1];
    detail = tabs[0];
    return Scaffold(
      appBar: AppBar(
        title: Text(args[0]),
        centerTitle: true,
        elevation: 1,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          itemCount: tabs.length,
          itemBuilder: (BuildContext context, int index) {
            int i = index;
            return Column(
              children: [
                SizedBox(height: ht),
                MyOutlinedButton(
                  width: btnwidth,
                  label: tabs[i]['info'],
                  onTap: () {
                    Navigator.pushNamed(context, '/information', arguments: tabs[i]);
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class Information extends StatelessWidget {
  Map<String, dynamic> _detail;
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments;
    _detail = args;
    return Scaffold(
      appBar: AppBar(
        title: Text(_detail['info']),
        centerTitle: true,
        elevation: 1,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(80),
                child: Image(
                  fit: BoxFit.fitHeight,
                  image: AssetImage(
                    _detail['image'],
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Text(
                _detail['description'],
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: 17,
                  letterSpacing: 0.017,
                ),
              ),
            ),
            Container(
              child: Text(
                _detail['information'],
                style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: 22,
                  letterSpacing: 0.022,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 40)
          ],
        ),
      ),
    );
  }
}
