import 'package:expandable/expandable.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

const ht = 20.0;
const btnwidth = 230.0;

class DietPlan extends StatefulWidget {
  @override
  _DietPlanState createState() => _DietPlanState();
}

class _DietPlanState extends State<DietPlan> {
  var dietPlan = {
    "title": "Activity Levels",
    "data": [
      "1.2 points for a person who does little to no exercise",
      "1.37 points for a slightly active person who does light exercise 1–3 days a week",
      "1.55 points for a moderately active person who performs moderate exercise 3–5 days a week",
      "1.725 points for a very active person who exercises hard 6–7 days a week",
      "1.9 points for an extra active person who either has a physically demanding job or has a particularly challenging exercise routine"
    ]
  };

  final ageController = TextEditingController();

  final weightController = TextEditingController();

  final heightController = TextEditingController();

  String gender = "Male";
  String activityLevel = "1.2";
  double calories = 0.0;

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Calorie Calculator", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: ListView(
          children: [
            GroupBox(dietPlan),
            Form(
              key: formKey,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white38,
                ),
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    InputForSignInUp(
                      controller: ageController,
                      title: "Age:",
                      hint: "eg 22",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Age cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputDropDown(
                      title: "Gender:",
                      value: gender,
                      onChange: (String value) {
                        setState(() {
                          gender = value;
                        });
                      },
                      list: ["Male", "Female"],
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: weightController,
                      title: "Weight:",
                      hint: "eg 104 (in pound)",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Weight cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: heightController,
                      title: "Height:",
                      hint: "eg 300 (in inches)",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Height cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputDropDown(
                      title: "Activity Levels:",
                      value: activityLevel,
                      onChange: (String value) {
                        setState(() {
                          activityLevel = value;
                        });
                      },
                      list: ["1.2", "1.37", "1.55", "1.725", "1.9"],
                    ),
                    SizedBox(height: 30),
                    Text(
                      calories.toStringAsFixed(calories.truncateToDouble() == calories ? 0 : 2) + "  calories/day",
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 30),
                    MyOutlinedButton(
                      label: "Calculate",
                      onTap: () {
                        if (formKey.currentState.validate()) {
                          int age = int.parse(ageController.text);

                          int weight = int.parse(weightController.text);
                          int height = int.parse(heightController.text);
                          double activityLevels = double.parse(activityLevel);
                          double bmr;

                          if (gender == 'Male')
                            bmr = 66 + (6.2 * weight) + (12.7 * height) - (6.76 * age);
                          else
                            bmr = 655.1 + (4.35 * weight) + (4.7 * height) - (4.7 * age);
                          double calorieBurnt = bmr * activityLevels;
                          print(calorieBurnt);
                          setState(() {
                            calories = calorieBurnt;
                          });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GroupBox extends StatelessWidget {
  Map<String, dynamic> data;
  GroupBox(this.data);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white38,
      ),
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(5),
      child: ExpandablePanel(
        header: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              data['title'],
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, fontFamily: "Nunito"),
            ),
          ),
        ),
        expanded: Container(
          child: data['data'].length == 0
              ? SizedBox()
              : Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.white38,
                  ),
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: ListView.builder(
                    itemCount: data['data'].length,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        child: Text(
                          data['data'][index],
                          style: TextStyle(color: Theme.of(context).accentColor, fontFamily: "Nunito", fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      );
                    },
                  ),
                ),
        ),
        collapsed: SizedBox(),
      ),
    );
  }
}
