import 'dart:convert';

import 'package:adobe_xd/adobe_xd.dart';
import 'package:fga/AttendanceDetail/AttendanceDetail.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/Enquiry/Enquiry.dart';
import 'package:fga/Home/Home.dart';
import 'package:fga/Information/Detailing.dart';
import 'package:fga/Information/Info.dart';
import 'package:fga/SignIn/SignIn.dart';
import 'package:fga/Signup/Focus.dart';
import 'package:fga/Signup/Gender.dart';
import 'package:fga/Signup/Goal.dart';
import 'package:fga/Signup/Level.dart';
import 'package:fga/Signup/SignUP.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

import 'Plans/Plans.dart';

const colorCode = 0xffA0D8FB;
Map<int, Color> color = {
  50: Color(colorCode).withOpacity(.1),
  100: Color(colorCode).withOpacity(.2),
  200: Color(colorCode).withOpacity(.3),
  300: Color(colorCode).withOpacity(.4),
  400: Color(colorCode).withOpacity(.5),
  500: Color(colorCode).withOpacity(.6),
  600: Color(colorCode).withOpacity(.7),
  700: Color(colorCode).withOpacity(.8),
  800: Color(colorCode).withOpacity(.9),
  900: Color(colorCode).withOpacity(1),
};
MaterialColor colorMain = MaterialColor(colorCode, color);
var API_URL = "";

Future<void> main() async {
  await DotEnv.load(fileName: ".env");
  runApp(MaterialApp(
    theme: ThemeData(
      primarySwatch: colorMain,
      accentColor: Colors.black,
      backgroundColor: colorMain,
    ),
    initialRoute: "/splash",
    routes: {
      "/splash": (context) => Splash(),
      "/signup": (context) => SignUp(),
      "/signin": (context) => SignIn(),
      "/gender": (context) => GenderSelection(),
      "/goal": (context) => Goal(),
      "/level": (context) => Level(),
      "/focus": (context) => FocusScreen(),
      "/home": (context) => Home(),
      "/info": (context) => Info(),
      "/infodetail": (context) => Detailing(),
      "/information": (context) => Information(),
      "/plans": (context) => Plans(),
      "/enquiry": (context) => Enquiry(),
      "/attendance": (context) => AttendanceDetail(),
    },
  ));
}

// Future<dynamic> loadFromFuture(context) async {
//   await showDialog(
//       context: context,
//       builder: (context) {
//         return Dialog(insetPadding: EdgeInsets.all(0), child: SingleChildScrollView(child: AddIPBox()));
//       });
//   // var result = await Future.delayed(Duration(milliseconds: 4000), () async {
//   //   try {
//   //     SharedPreferences prefs = await SharedPreferences.getInstance();
//   //     String key = await prefs.getString('token');

//   //     if (key == null || key == "") return Future.value(Navigator.popAndPushNamed(context, '/signin'));
//   //     return Future.value(Navigator.popAndPushNamed(context, '/home', arguments: key));
//   //   } catch (e) {
//   //     print(e);
//   //     return Future.value(Navigator.popAndPushNamed(context, '/signin'));
//   //   }
//   // });
//   // return result;
// }

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final ipController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  void saveMember() async {
    API_URL = "http://" + ipController.text + ":8000/";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = await prefs.getString('token');

    if (key == null || key == "") Navigator.popAndPushNamed(context, '/signin');
    Navigator.popAndPushNamed(context, '/home', arguments: key);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        padding: EdgeInsets.all(20),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 40),
                Image(
                  image: AssetImage('assets/images/logo.png'),
                  height: 200.0,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 30),
                  margin: EdgeInsets.all(0),
                  decoration: BoxDecoration(
                    color: Colors.white54,
                    border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.1)),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    children: [
                      InputForSignInUp(
                        controller: ipController,
                        title: "Hosted IP address:",
                        hint: "eg. 127.0.0.1",
                        validator: (value) {
                          if (value == null || value == '') return "IP address cannot be empty";
                        },
                        keybordType: TextInputType.text,
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                MyOutlinedButton(
                  label: "Save",
                  onTap: () {
                    if (_formKey.currentState.validate()) saveMember();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );

    // return SplashScreen(
    //   navigateAfterFuture: loadFromFuture(context),
    //   image: Image.asset('assets/images/logo.png'),
    //   backgroundColor: Theme.of(context).primaryColor,
    //   styleTextUnderTheLoader: new TextStyle(),
    //   photoSize: 100.0,
    //   useLoader: false,
    // );
  }
}

class AddIPBox extends StatefulWidget {
  Function reload;
  AddIPBox({this.reload});
  @override
  _AddMemberBoxState createState() => _AddMemberBoxState();
}

class _AddMemberBoxState extends State<AddIPBox> {
  final ipController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  void saveMember() async {
    API_URL = ipController.text;
    Navigator.pop(context);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = await prefs.getString('token');

    if (key == null || key == "") return Future.value(Navigator.popAndPushNamed(context, '/signin'));
    return Future.value(Navigator.popAndPushNamed(context, '/home', arguments: key));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.transparent,
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(0),
                margin: EdgeInsets.all(0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.1)),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  children: [
                    InputForSignInUp(
                      controller: ipController,
                      title: "Hosted IP address:",
                      hint: "eg. 127.0.0.1",
                      validator: (value) {
                        if (value == null || value == '') return "IP address cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              SizedBox(height: 10),
              MyOutlinedButton(
                label: "Save",
                onTap: () {
                  if (_formKey.currentState.validate()) saveMember();
                },
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
