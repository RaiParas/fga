import 'dart:convert';
import 'package:email_validator/email_validator.dart';
import 'package:expandable/expandable.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/CustomWidgets/Picker.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:fga/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

const ht = 20.0;
const btnwidth = 230.0;

class Members extends StatefulWidget {
  @override
  _MembersState createState() => _MembersState();
}

class _MembersState extends State<Members> {
  var members = [];

  Future<void> loadData() async {
    var url = Uri.parse(API_URL + 'api/member/');
    print(API_URL);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = await prefs.getString('token');
    http
        .get(url, headers: {"Accept": "application/json", "authentication": "Basic $key"})
        .then((response) async {
          print(response.statusCode);
          if (response.statusCode == 200) {
            setState(() {
              members = jsonDecode(response.body);
            });
          } else {
            print(jsonDecode(response.body)['detail']);
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          Fluttertoast.showToast(
            msg: "Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  Future<void> delData(String id) async {
    print(id);
    var url = Uri.parse(API_URL + 'api/member/$id/');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = await prefs.getString('token');
    http
        .delete(url, headers: {"Accept": "application/json", "authentication": "Basic $key"})
        .then((response) async {
          print(response.statusCode);
          if (response.statusCode == 204) {
            loadData();
            Fluttertoast.showToast(
              msg: "Member deleted",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.deepOrange,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          } else {
            loadData();
            Fluttertoast.showToast(
              msg: "Member not deleted",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 10000))
        .catchError((err) {
          loadData();
          Fluttertoast.showToast(
            msg: "Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Members", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: Icon(CupertinoIcons.add_circled),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return Dialog(insetPadding: EdgeInsets.all(0), child: SingleChildScrollView(child: AddMemberBox(reload: loadData)));
                  });
            },
          ),
        ],
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            // Container(
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   margin: EdgeInsets.all(20),
            //   decoration: BoxDecoration(
            //     borderRadius: BorderRadius.circular(10),
            //     color: Colors.white30,
            //   ),
            //   child: Row(
            //     children: [
            //       Expanded(
            //         child: TextField(
            //           decoration: InputDecoration(hintText: "Search", border: InputBorder.none),
            //           cursorColor: Theme.of(context).accentColor,
            //         ),
            //       ),
            //       IconButton(icon: Icon(Icons.search), onPressed: () async {}),
            //     ],
            //   ),
            // ),
            Expanded(
              child: ListView.builder(
                itemCount: members.length,
                itemBuilder: (context, index) {
                  return MemberDetailBox(data: members[index], delFunc: delData);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MemberDetailBox extends StatelessWidget {
  Map<String, dynamic> data;
  Function delFunc;
  MemberDetailBox({this.data, this.delFunc});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: Colors.white38,
      ),
      margin: EdgeInsets.symmetric(vertical: 2),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      child: ExpandablePanel(
        header: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            data['name'],
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, fontFamily: "Nunito"),
          ),
        ),
        expanded: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white38,
          ),
          margin: EdgeInsets.symmetric(horizontal: 5),
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              MemberDetail(title: "Contact", info: data['contact']),
              MemberDetail(title: "Email", info: data['emailid']),
              MemberDetail(title: "Age", info: data['age']),
              MemberDetail(title: "Gender", info: data['gender']),
              MemberDetail(title: "Plan", info: data['plan']),
              MemberDetail(title: "Join date", info: data['joindate']),
              MemberDetail(title: "Expire date", info: data['expiredate']),
              MemberDetail(title: "Initial amount", info: data['initialamount']),
              TextButton(
                child: Text(
                  "Delete",
                  style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Delete member " + data['name'] + "?"),
                        content: Text("Want to delete member?"),
                        actions: [
                          TextButton(
                            child: Text("Yes", style: TextStyle(color: Colors.red)),
                            onPressed: () {
                              Navigator.pop(context);
                              delFunc(data['id'].toString());
                            },
                          ),
                          TextButton(
                            child: Text("No"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            ],
          ),
        ),
        collapsed: SizedBox(),
      ),
    );
  }
}

class MemberDetail extends StatelessWidget {
  String title, info;
  MemberDetail({this.title, this.info});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            title,
            style: TextStyle(color: Theme.of(context).accentColor, fontFamily: "Nunito", fontWeight: FontWeight.bold),
          ),
          Text(
            info,
            style: TextStyle(
              fontSize: 12,
              color: Theme.of(context).accentColor,
              fontFamily: "Nunito",
            ),
          ),
        ],
      ),
    );
  }
}

class AddMemberBox extends StatefulWidget {
  Function reload;
  AddMemberBox({this.reload});
  @override
  _AddMemberBoxState createState() => _AddMemberBoxState();
}

class _AddMemberBoxState extends State<AddMemberBox> {
  final emailController = TextEditingController();

  final passwordController = TextEditingController();

  final nameController = TextEditingController();

  final contactController = TextEditingController();

  final ageController = TextEditingController();

  String joinDate;

  String expireDate;

  final initAmountController = TextEditingController();

  String gender = "Male";

  String planname = "Build Muscle";

  bool attendence = true;

  final _formKey = GlobalKey<FormState>();

  Future<void> saveMember() async {
    var url = Uri.parse(API_URL + 'api/member/');

    Fluttertoast.showToast(
      msg: "Saving",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.orange,
      textColor: Colors.white,
      fontSize: 12.0,
    );

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = prefs.getString('token');
    http
        .post(url, headers: {
          "Accept": "application/json",
          "Authentication": "Basic $key"
        }, body: {
          "name": nameController.text,
          "contact": contactController.text,
          "emailid": emailController.text,
          "age": ageController.text,
          "gender": gender,
          "plan": planname,
          "joindate": joinDate,
          "expiredate": expireDate,
          "initialamount": initAmountController.text.toString(),
          "attendance": attendence ? "1" : "2"
        })
        .then((response) async {
          if (response.statusCode == 201) {
            widget.reload();
            Fluttertoast.showToast(
              msg: "Member added",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 12.0,
            );
            Navigator.pop(context);
          } else {
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          Fluttertoast.showToast(
            msg: "Error loading data",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.transparent,
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(0),
                margin: EdgeInsets.all(0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.1)),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 30),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Add member",
                        style: TextStyle(fontFamily: "Nunito", fontWeight: FontWeight.bold, fontSize: 17),
                      ),
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: nameController,
                      title: "Name:",
                      hint: "eg. hero man",
                      validator: (value) {
                        if (value == null || value == '') return "Name cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: contactController,
                      title: "Contact:",
                      hint: "eg. hero man",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Contact cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: emailController,
                      title: "Email:",
                      hint: "eg. eg@email.com",
                      keybordType: TextInputType.emailAddress,
                      validator: (value) {
                        if (!EmailValidator.validate(value)) return "Email not valid";
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: ageController,
                      title: "Age:",
                      hint: "eg. 21",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Age cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputDropDown(
                      title: "Gender:",
                      value: gender,
                      onChange: (String value) {
                        setState(() {
                          gender = value;
                        });
                      },
                      list: ["Male", "Female"],
                    ),
                    SizedBox(height: 10),
                    InputDropDown(
                      title: "Plan Name:",
                      value: planname,
                      onChange: (String value) {
                        setState(() {
                          planname = value;
                        });
                      },
                      list: ["Build Muscle", "Loose Weight"],
                    ),
                    SizedBox(height: 10),
                    DatePicker(
                      value: this.joinDate,
                      title: "Join Date:",
                      hint: "eg. 2021/05/06",
                      onConfirm: (date) {
                        setState(() {
                          joinDate = DateFormat("yyyy-MM-dd").format(date);
                        });
                      },
                    ),
                    SizedBox(height: 10),
                    DatePicker(
                      value: expireDate,
                      title: "Expire Date:",
                      hint: "eg. 2023/05/06",
                      onConfirm: (date) {
                        setState(() {
                          expireDate = DateFormat("yyyy-MM-dd").format(date);
                        });
                      },
                    ),
                    SizedBox(height: 10),
                    InputForSignInUp(
                      controller: initAmountController,
                      title: "Initial Amount",
                      hint: "eg. 1000",
                      keybordType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value == '') return "Initial amount cannot be empty";
                      },
                    ),
                    SizedBox(height: 10),
                    InputDropDown(
                      title: "Attendance:",
                      value: attendence ? "True" : "False",
                      onChange: (String value) {
                        setState(() {
                          attendence = value == "True" ? true : false;
                        });
                      },
                      list: ["True", "False"],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              SizedBox(height: 10),
              MyOutlinedButton(
                label: "Submit",
                onTap: () {
                  if (_formKey.currentState.validate()) saveMember();
                },
              ),
              SizedBox(height: 10),
              MyOutlinedButton(
                label: "Cancel",
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
