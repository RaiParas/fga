import 'dart:convert';

import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/main.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  void login() {
    Fluttertoast.showToast(
        msg: "Logging in",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 12.0);
    var url = Uri.parse(API_URL + 'api/rest-auth/login/');
    http
        .post(url,
            headers: {"Accept": "application/json", "Authorization": "Basic e1cfaa696ed51bf1e007074626822a8669cb1abb"},
            body: {'username': usernameController.text, 'password': passwordController.text})
        .then((response) async {
          if (response.statusCode == 200) {
            Fluttertoast.showToast(
              msg: "Logged in",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 12.0,
            );
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String key = jsonDecode(response.body)['key'];
            await prefs.setString('token', key);
            Navigator.popAndPushNamed(context, '/home', arguments: key);
          } else {
            Fluttertoast.showToast(
              msg: (response.body),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
          print('Response status: ${response.statusCode}');
          print('Response body: ${response.body}');
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          Fluttertoast.showToast(
            msg: "Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.orange,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  bool isAgree = false;
  final passwordController = TextEditingController();
  final usernameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign In"),
        elevation: 1,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Center(
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width > 400 ? 400 : double.infinity,
            child: Form(
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Image.asset(
                    'assets/images/logo.png',
                    height: 180,
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.1)),
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        InputForSignInUp(
                          controller: usernameController,
                          title: "Username:",
                          hint: "eg. hero",
                        ),
                        SizedBox(height: 10),
                        InputForSignInUp(
                          controller: passwordController,
                          title: "Password:",
                          hint: "••••••••",
                          keybordType: TextInputType.visiblePassword,
                          onsecuretext: true,
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [TextButton(onPressed: () {}, child: Text("Forget password?"))],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  MyOutlinedButton(
                    label: "Sign In",
                    onTap: () {
                      login();
                    },
                  ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Don't have an account? ", style: TextStyle(fontFamily: "Nunito")),
                      TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/signup');
                          },
                          child: Text("Sign Up Now", style: TextStyle(color: Theme.of(context).accentColor.withOpacity(0.4))))
                    ],
                  ),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ),
        ),
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }
}
