import 'package:fga/Account/Account.dart';
import 'package:fga/DietPlan/DietPlan.dart';
import 'package:fga/Information/Info.dart';
import 'package:fga/Members/Members.dart';
import 'package:fga/Settings/Settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _selectedIndex = 2;

  String token;
  var _pages = [
    {"title": "Calorie Calculator ", "page": DietPlan(), "actions": []},
    {"title": "Members", "page": Members(), "actions": []},
    {"title": "Attendance", "page": Account(), "actions": []},
    {"title": "Settings", "page": Settings(), "actions": []},
    {"title": "Information", "page": Info(), "actions": []}
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final key = ModalRoute.of(context).settings.arguments as String;
    token = key;
    return Scaffold(
      body: _pages[_selectedIndex]['page'],
      backgroundColor: Theme.of(context).primaryColor,
      bottomNavigationBar: BottomNavigationBar(
        elevation: 4,
        backgroundColor: Theme.of(context).primaryColor,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.fastfood, color: Theme.of(context).accentColor), label: 'Diet Calculation'),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.person_3_fill, color: Theme.of(context).accentColor), label: 'Members'),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.qrcode, color: Theme.of(context).accentColor), label: 'Attendence'),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.gear_alt_fill, color: Theme.of(context).accentColor), label: 'Settings'),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.ellipsis_vertical, color: Theme.of(context).accentColor), label: 'Information'),
        ],
        currentIndex: _selectedIndex,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
