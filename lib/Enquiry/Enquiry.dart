import 'dart:convert';
import 'package:email_validator/email_validator.dart';
import 'package:expandable/expandable.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/CustomWidgets/InputBox.dart';
import 'package:fga/CustomWidgets/Picker.dart';
import 'package:fga/CustomWidgets/SelectionButtons.dart';
import 'package:fga/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

const ht = 20.0;
const btnwidth = 230.0;

class Enquiry extends StatefulWidget {
  @override
  _EnquiryState createState() => _EnquiryState();
}

class _EnquiryState extends State<Enquiry> {
  var plans = [];

  Future<void> loadData() async {
    var url = Uri.parse(API_URL + 'api/enquiry/');
    print("");

    http
        .get(url, headers: {"Accept": "application/json"})
        .then((response) async {
          print(response.body);
          if (response.statusCode == 200) {
            setState(() {
              plans = jsonDecode(response.body);
            });
          } else {
            print(jsonDecode(response.body)['detail']);
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 4000))
        .catchError((err) {
          print(err);
          if ("" != "")
            Fluttertoast.showToast(
              msg: "Server Error",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
        });
  }

  Future<void> delData(String id) async {
    var url = Uri.parse(API_URL + 'api/enquiry/$id/');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = await prefs.getString('token');
    http
        .delete(url, headers: {"Accept": "application/json", "Authentication": "Basic $key"})
        .then((response) async {
          print(response.body);
          if (response.statusCode == 204) {
            loadData();
            Fluttertoast.showToast(
              msg: "Enquiry deleted",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.deepOrange,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          } else {
            loadData();
            Fluttertoast.showToast(
              msg: "Enquiry not deleted",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 10000))
        .catchError((err) {
          loadData();
          Fluttertoast.showToast(
            msg: "Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Enquiry", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          itemCount: plans.length,
          itemBuilder: (context, index) {
            return EnquiryDetailBox(plans[index], delData);
          },
        ),
      ),
    );
  }
}

class EnquiryDetailBox extends StatelessWidget {
  Map<String, dynamic> data;
  Function delFunc;
  EnquiryDetailBox(this.data, this.delFunc);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: Colors.white38,
      ),
      margin: EdgeInsets.symmetric(vertical: 2),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      child: ExpandablePanel(
        header: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            data['name'],
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, fontFamily: "Nunito"),
          ),
        ),
        expanded: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white38,
          ),
          margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              EnquiryDetail(title: "contact", info: data['contact']),
              EnquiryDetail(title: "emailid", info: data['emailid']),
              EnquiryDetail(title: "age", info: data['age']),
              EnquiryDetail(title: "gender", info: data['gender']),
              TextButton(
                child: Text(
                  "Delete",
                  style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Delete member " + data['name'] + "?"),
                        content: Text("Want to delete member?"),
                        actions: [
                          TextButton(
                            child: Text("Yes", style: TextStyle(color: Colors.red)),
                            onPressed: () {
                              Navigator.pop(context);
                              delFunc(data['id'].toString());
                            },
                          ),
                          TextButton(
                            child: Text("No"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            ],
          ),
        ),
        collapsed: SizedBox(),
      ),
    );
  }
}

class EnquiryDetail extends StatelessWidget {
  String title, info;
  EnquiryDetail({this.title, this.info});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            title,
            style: TextStyle(color: Theme.of(context).accentColor, fontFamily: "Nunito", fontWeight: FontWeight.bold),
          ),
          Text(
            info,
            style: TextStyle(
              fontSize: 12,
              color: Theme.of(context).accentColor,
              fontFamily: "Nunito",
            ),
          ),
        ],
      ),
    );
  }
}
