import 'package:esewa_pnp/esewa.dart';
import 'package:esewa_pnp/esewa_pnp.dart';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

const ht = 20.0;
const btnwidth = 230.0;

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Settings", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            ProfileButton(
              icon: Icons.payment,
              label: "Pay with eSewa",
              onTap: () async {
                ESewaConfiguration _configuration = ESewaConfiguration(
                  clientID: "JB0BBQ4aD0UqIThFJwAKBgAXEUkEGQUBBAwdOgABHD4DChwUAB0R",
                  secretKey: "BhwIWQQADhIYSxILExMcAgFXFhcOBwAKBgAXEQ==",
                  environment: ESewaConfiguration.ENVIRONMENT_TEST, //ENVIRONMENT_LIVE
                );
                ESewaPnp _eSewaPnp = ESewaPnp(configuration: _configuration);
                ESewaPayment _payment = ESewaPayment(amount: 10, productName: "FGA membership", productID: "#1234", callBackURL: "");
                final _res = await _eSewaPnp.initPayment(payment: _payment);
                print(_res);

                Fluttertoast.showToast(
                  msg: "Payment done",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.green,
                  textColor: Colors.white,
                  fontSize: 18.0,
                );
              },
            ),
            ProfileButton(
              icon: CupertinoIcons.question_circle,
              label: "Enqury",
              onTap: () {
                Navigator.pushNamed(context, '/enquiry');
              },
            ),
            ProfileButton(
              icon: Icons.inbox,
              label: "Plans",
              onTap: () {
                Navigator.pushNamed(context, '/plans');
              },
            ),
            ProfileButton(
              icon: Icons.logout,
              label: "Logout",
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                await prefs.clear();
                return Future.value(Navigator.popAndPushNamed(context, '/signin'));
              },
            ),
          ],
        ),
      ),
    );
  }
}
