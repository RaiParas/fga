import 'dart:convert';
import 'dart:io';
import 'package:fga/CustomWidgets/Buttons.dart';
import 'package:fga/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

import 'package:shared_preferences/shared_preferences.dart';

const ht = 20.0;
const btnwidth = 230.0;

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  permission() async {
    await Permission.camera.request();
  }

  Future<void> attend(String number) async {
    DateTime today = new DateTime.now();
    int year = today.year;
    int month = today.month;
    int day = today.day;
    var url = Uri.parse(API_URL + 'api/attendance-report/');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = prefs.getString('token');
    http
        .post(url, headers: {
          "Accept": "application/json",
          "Content": "application/json",
          "authentication": "Basic $key"
        }, body: {
          "year": (year.toString()),
          "month": (month.toString()),
          "day": (day.toString()),
          "name": number.toString(),
          "status": "True",
        })
        .then((response) async {
          if (response.statusCode == 201) {
            Fluttertoast.showToast(
              msg: "Attendence completed",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          } else {
            print(jsonDecode(response.body)['detail']);
            Fluttertoast.showToast(
              msg: (jsonDecode(response.body)['detail']),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 12.0,
            );
          }
        })
        .timeout(Duration(milliseconds: 10000))
        .catchError((err) {
          Fluttertoast.showToast(
            msg: "Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        });
  }

  @override
  void initState() {
    super.initState();
    permission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text("Attendance", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, fontFamily: "Nunito")),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            SizedBox(height: 80),
            Text(
              "Please scan your QR here",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            MyOutlinedButton(
              label: "View attendance",
              onTap: () {
                Navigator.pushNamed(context, "/attendance");
              },
            ),
            SizedBox(height: 120),
            Expanded(
              child: Container(
                height: double.infinity,
                child: Center(
                  child: InkWell(
                    splashColor: Theme.of(context).primaryColor,
                    highlightColor: Colors.white30,
                    borderRadius: BorderRadius.circular(90),
                    onLongPress: () async {
                      var status = false;
                      if (Platform.isAndroid) {
                        var statusAndroid = await Permission.camera.request();
                        status = statusAndroid.isGranted;
                      } else {
                        status = true;
                      }
                      if (status) {
                        String cameraScanResult = await scanner.scan();

                        attend(cameraScanResult);
                      }
                    },
                    child: Container(
                      height: 140,
                      width: 140,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.fingerprint, size: 80),
                            SizedBox(height: 10),
                            Text(
                              "Long press to attend",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10, color: Theme.of(context).accentColor),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
