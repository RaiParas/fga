import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

double borderRad = 10;

class SelectionButton extends StatelessWidget {
  String title, subtitle;
  double width, height;
  bool isSelected, small;
  Function onTap;
  SelectionButton({Key key, this.title, this.width = 100, this.subtitle = null, this.isSelected = false, this.onTap, this.small = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: this.isSelected ? Theme.of(context).accentColor.withOpacity(0.3) : Colors.white,
      borderRadius: BorderRadius.circular(borderRad),
      child: InkWell(
        onTap: this.onTap,
        enableFeedback: true,
        borderRadius: BorderRadius.circular(borderRad),
        splashColor: Theme.of(context).accentColor,
        child: Container(
          width: this.width,
          padding: EdgeInsets.only(top: 10, bottom: this.subtitle == null ? 10 : 0),
          alignment: Alignment.center,
          decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor), borderRadius: BorderRadius.circular(borderRad)),
          child: Column(
            children: [
              Text(
                this.title,
                style: TextStyle(fontSize: small ? 12 : 20, fontWeight: small ? FontWeight.normal : FontWeight.bold, fontFamily: "Nunito"),
              ),
              this.subtitle == null
                  ? SizedBox(height: 0)
                  : Text(
                      this.subtitle,
                      style: TextStyle(fontSize: small ? 9 : 12, fontWeight: FontWeight.w100, fontFamily: "Nunito"),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
