import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

double borderRad = 13;

// ignore: must_be_immutable
class NextStepButton extends StatelessWidget {
  double width;
  bool isEnabled;
  Function onTap;
  NextStepButton({Key key, this.width = 130, this.isEnabled = false, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: !this.isEnabled ? Colors.white : Theme.of(context).accentColor.withOpacity(1),
      borderRadius: BorderRadius.circular(borderRad),
      child: InkWell(
        onTap: isEnabled ? this.onTap : null,
        enableFeedback: true,
        borderRadius: BorderRadius.circular(borderRad),
        splashColor: Theme.of(context).accentColor.withOpacity(0.8),
        highlightColor: Colors.white.withOpacity(0.5),
        child: Container(
          width: this.width,
          padding: EdgeInsets.symmetric(vertical: 8),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.7)), borderRadius: BorderRadius.circular(borderRad)),
          child: Text(
            "Next Step",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w300,
              fontFamily: "Nunito",
              color: this.isEnabled ? Colors.white : Theme.of(context).accentColor.withOpacity(0.7),
            ),
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class MyOutlinedButton extends StatelessWidget {
  double width;
  Function onTap;
  String label;
  MyOutlinedButton({Key key, this.label, this.width = 130, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      borderRadius: BorderRadius.circular(borderRad),
      child: InkWell(
        onTap: this.onTap,
        enableFeedback: true,
        borderRadius: BorderRadius.circular(borderRad),
        splashColor: Theme.of(context).accentColor.withOpacity(0.8),
        highlightColor: Colors.white.withOpacity(0.5),
        child: Container(
          width: this.width,
          padding: EdgeInsets.symmetric(vertical: 8),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.7)), borderRadius: BorderRadius.circular(borderRad)),
          child: Text(
            this.label,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w300,
              color: Theme.of(context).accentColor.withOpacity(0.7),
              fontFamily: "Nunito",
            ),
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class ProfileButton extends StatelessWidget {
  Function onTap;
  String label;
  IconData icon;
  ProfileButton({Key key, this.icon, this.label, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white.withOpacity(0.4),
      child: InkWell(
        onTap: this.onTap,
        enableFeedback: true,
        splashColor: Theme.of(context).accentColor.withOpacity(0.8),
        highlightColor: Colors.white.withOpacity(0.5),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
          alignment: Alignment.center,
          decoration:
              BoxDecoration(border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.2)), borderRadius: BorderRadius.circular(0)),
          child: Row(
            children: [
              Icon(this.icon),
              SizedBox(width: 16),
              Text(
                this.label,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).accentColor.withOpacity(0.7),
                  fontFamily: "Nunito",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
