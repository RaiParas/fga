import 'package:flutter/material.dart';

class InputForHW extends StatelessWidget {
  String title, hint;
  TextEditingController controller;
  TextInputType keybordType;
  InputForHW({this.title, this.hint, this.controller, this.keybordType = TextInputType.text});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            this.title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, fontFamily: "Nunito"),
          ),
          SizedBox(
            width: 30,
            child: TextField(
              style: TextStyle(fontFamily: "Nunito"),
              scrollPadding: EdgeInsets.all(0),
              cursorColor: Theme.of(context).accentColor,
              textAlign: TextAlign.right,
              controller: this.controller,
              decoration: InputDecoration(hintText: this.hint, border: InputBorder.none, contentPadding: EdgeInsets.symmetric(vertical: 0)),
              keyboardType: this.keybordType,
            ),
          ),
        ],
      ),
    );
  }
}

class InputForSignInUp extends StatelessWidget {
  String title, hint;
  TextEditingController controller;
  TextInputType keybordType;
  Function validator;
  bool onsecuretext;
  InputForSignInUp({this.title, this.hint, this.controller, this.keybordType = TextInputType.text, this.validator, this.onsecuretext = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            width: 70,
            child: Text(
              this.title,
              textAlign: TextAlign.right,
              style: TextStyle(fontWeight: FontWeight.w600, fontFamily: "Nunito"),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: TextFormField(
              scrollPadding: EdgeInsets.all(0),
              cursorColor: Theme.of(context).accentColor,
              controller: this.controller,
              style: TextStyle(fontFamily: "Nunito"),
              decoration: InputDecoration(
                hintText: this.hint,
                border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).primaryColor)),
                contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              ),
              keyboardType: this.keybordType,
              validator: this.validator,
              obscureText: this.onsecuretext,
            ),
          ),
        ],
      ),
    );
  }
}

class InputDropDown extends StatelessWidget {
  String title;
  List<String> list;
  String value;
  Function onChange;
  InputDropDown({this.title, this.list, this.value, this.onChange});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            width: 70,
            child: Text(
              this.title,
              textAlign: TextAlign.right,
              style: TextStyle(fontWeight: FontWeight.w600, fontFamily: "Nunito"),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: DropdownButton(
              value: this.value,
              onChanged: this.onChange,
              underline: SizedBox(),
              style: TextStyle(fontFamily: "Nunito", fontSize: 16, color: Theme.of(context).accentColor),
              items: this.list.map((String data) {
                return DropdownMenuItem<String>(
                  child: Text(data.toString()),
                  value: data.toString(),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
