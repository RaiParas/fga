import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as DT;
import 'package:intl/intl.dart';

class DatePicker extends StatelessWidget {
  String title, hint;
  TextEditingController controller;
  TextInputType keybordType;
  Function validator, onConfirm;
  bool onsecuretext;
  String value;
  DatePicker({this.title, this.hint, this.value, this.onConfirm});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            width: 70,
            child: Text(
              this.title,
              textAlign: TextAlign.right,
              style: TextStyle(fontWeight: FontWeight.w600, fontFamily: "Nunito"),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: TextButton(
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  this.value ?? this.hint,
                  style: TextStyle(color: Theme.of(context).accentColor, fontFamily: "Nunito"),
                ),
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                margin: EdgeInsets.all(0),
                decoration:
                    BoxDecoration(border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.4)), borderRadius: BorderRadius.circular(5)),
              ),
              onPressed: () {
                DT.DatePicker.showDatePicker(
                  context,
                  showTitleActions: true,
                  onChanged: (date) {},
                  onConfirm: onConfirm,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
